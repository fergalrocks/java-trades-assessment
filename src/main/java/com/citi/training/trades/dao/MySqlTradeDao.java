package com.citi.training.trades.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.trades.model.Trade;

/* REST Interface class for {@link com.citi.training.trade.dao} domain object.
 * 
 * @author Administrator
 * @see TradeDao 
 * @see <a href="http://www.google.com">Google</a>
 *
 */

@Component
public class MySqlTradeDao implements TradesDao {

	@Autowired
	JdbcTemplate tpl;

	/**
	 * Find all {@link com.citi.training.trades.dao.TradesDao} in the database.
	 * 
	 * @return all {@link com.citi.training.trades.dao.Trades.dao} that were found
	 *         or HTTP 404.
	 */
	public List<Trade> findAll() {
		return tpl.query("SELECT id, stock, price, volume FROM trade", new TradeMapper());
	}

	/**
	 * Delete an {@link com.citi.training.trades.dao.TradesDao} by its integer id.
	 * 
	 * @param id The id of the trade to delete
	 * @return {@link com.citi.training.trade.dao.Trade.dao} that was deleted or
	 *         HTTP 404.
	 */
	public void deleteById(int id) {
		findById(id);
		tpl.update("DELETE FROM trade WHERE id=?", id);
	}

	/**
	 * Find an {@link com.citi.training.trades.dao.TradesDao} by it's integer id.
	 * 
	 * @param id The id of the trade to find
	 * @return {@link com.citi.training.trade.dao.Trade.dao} that was found or HTTP
	 *         404.
	 */
	public Trade findById(int id) {
		List<Trade> trade = tpl.query("SELECT id, stock, price, volume FROM trade WHERE id=?", new Object[] { id },
		        new TradeMapper());
		return trade.get(0);
	}

	/**
	 * Create an {@link com.citi.training.trades.dao.TradesDao}
	 * 
	 * @return {@link com.citi.training.trades.dao.TradesDao} that was created or
	 *         HTTP 404.
	 */
	public Trade create(Trade trade) {
		KeyHolder keyHolder = new GeneratedKeyHolder();

		tpl.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {

				PreparedStatement ps = connection.prepareStatement(
				        "insert into trade (id, stock,price, volume) values (?, ?, ?, ?)",
				        Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, trade.getId());
				ps.setString(2, trade.getStock());
				ps.setDouble(3, trade.getPrice());
				ps.setInt(4, trade.getVolume());
				return ps;
			}
		}, keyHolder);
		trade.setId(keyHolder.getKey().intValue());
		return trade;
	}

	private static final class TradeMapper implements RowMapper<Trade> {

		public Trade mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Trade(rs.getInt("id"), rs.getString("stock"), rs.getDouble("price"), rs.getInt("volume"));
		}

	}

}
