package com.citi.training.trades.dao;

import java.util.List;

import com.citi.training.trades.model.Trade;

public interface TradesDao {

	List<Trade> findAll();

	Trade findById(int id);

	Trade create(Trade trade);

	void deleteById(int id);
}
